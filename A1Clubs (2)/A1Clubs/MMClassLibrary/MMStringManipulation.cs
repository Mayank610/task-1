﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace MMClassLibrary
{
   public static class MMStringManipulation
    {
        #region Validation Methods
        /// <summary>
        /// to extract digits from the string
        /// </summary>
        /// <param name="digits">string data</param>
        /// <returns></returns>
        public static string MMExtraDigits(string digits)
        {
            if (string.IsNullOrWhiteSpace(digits))
            {
                return "";
            }
            else
            {
                string numbers = "";
                for(var i = 0; i < digits.Length; i++)
                {
                    if (Char.IsDigit(digits[i]))
                    {
                        numbers += digits[i];
                    }
                }
                return numbers;
            }
        }

        /// <summary>
        /// to validate the postal code
        /// </summary>
        /// <param name="postalCode">postal code</param>
        /// <param name="pcRegex">regexpr pattern for postal code</param>
        /// <returns></returns>
        public static bool MMPostalCodeIsValid(string postalCode,string pcRegex)
        {
            Regex postalCodeRegex = new Regex(pcRegex);
            if ((postalCodeRegex.IsMatch(postalCode)) || string.IsNullOrWhiteSpace(pcRegex))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// to capitalize the first letter of each word
        /// </summary>
        /// <param name="Name">string to capitalize</param>
        /// <returns></returns>
        public static string MMCapitalize(string Name)
        {
            
             if (!string.IsNullOrWhiteSpace(Name))
             {
                Name = Name.ToLower();
                var CapName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
                CapName = CapName.Trim();
                
                 return CapName;
             }
             else
             {
                 return "";
             }
            
        }
        /// <summary>
        /// check EmailValidation from given string
        /// </summary>
        /// <param name="EmailId">string data</param>
        /// <returns></returns>
        public static bool MMEmailAnnotations(string EmailId)
        {
            if (!string.IsNullOrWhiteSpace(EmailId))
            {
                Regex regEm = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
                return regEm.IsMatch(EmailId);
            }
            else
            {
                return false;
            }
                    
        }
        #endregion
    }
}
