﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MMClassLibrary;

namespace A1Clubs.Models.MetaDataClasses
{
    public class MMNameAddressMetaData : IValidatableObject
    {
        ClubsContext _context = new ClubsContext();


        public int NameAddressId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string ProvinceCode { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [MinLength(10, ErrorMessage = "phone must have exactly 10 digits")]
        [MaxLength(10, ErrorMessage = "phone must have exactly 10 digits")]
        public string Phone { get; set; }

        public virtual Province ProvinceCodeNavigation { get; set; }
        public virtual Club Club { get; set; }
        public virtual ICollection<Artist> Artist { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(FirstName) && string.IsNullOrWhiteSpace(LastName) && string.IsNullOrWhiteSpace(CompanyName))
            {
                yield return new ValidationResult("at least one of firstname,lastname or company name must be provided", new List<string> { "Err1" });
            }
            if (string.IsNullOrWhiteSpace(Email))
            {
                if (string.IsNullOrWhiteSpace(City) || string.IsNullOrWhiteSpace(StreetAddress) || string.IsNullOrWhiteSpace(PostalCode))
                {
                    yield return new ValidationResult("all postal information is required if email is not provided", new List<string> { "Err2" });
                }
            }

            var provinceData = _context.Province.FirstOrDefault(p => p.ProvinceCode == ProvinceCode);

          
                var countryData = _context.Country.FirstOrDefault(p => p.CountryCode == (provinceData == null ? null : provinceData.CountryCode));
            
            if (!string.IsNullOrWhiteSpace(ProvinceCode))
            {

                if (provinceData == null)
                {
                    yield return new ValidationResult("province not found", new List<string> { "Err3" });
                }



                if (!string.IsNullOrWhiteSpace(PostalCode))
                {
                    var pcode = PostalCode.ToCharArray();



                    if (pcode[0].ToString().ToUpper() != provinceData.FirstPostalLetter || !MMStringManipulation.MMPostalCodeIsValid(PostalCode, countryData.PostalPattern))
                    {
                        yield return new ValidationResult("postal code invalid for the given province code", new List<string> { "Err4" });
                    }
                }

              /*  if (!MMStringManipulation.MMPostalCodeIsValid(PostalCode, countryData.PostalPattern))
                {
                    yield return new ValidationResult("postal code invalid for the given province code", new List<string> { "Err4" });
                }*/

                if(!MMStringManipulation.MMEmailAnnotations(Email))
                {
                    yield return new ValidationResult("please provide valid email address", new List<string> { "Err5" });
                }

            }


        }
    }
}
