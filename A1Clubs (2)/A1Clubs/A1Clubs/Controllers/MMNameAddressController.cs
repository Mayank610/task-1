﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using A1Clubs.Models;
using A1Clubs.Models.MetaDataClasses;
using MMClassLibrary;

namespace A1Clubs.Controllers
{
    public class MMNameAddressController : Controller
    {
        #region Private Object
        private readonly ClubsContext _context;
        #endregion
        #region Constructor
        public MMNameAddressController(ClubsContext context)
        {
            _context = context;
        }
        #endregion
        #region Action

        // GET: MMNameAddress
        public async Task<IActionResult> Index()
        {
            TempData["SuccessMsg"] = null;
            var clubsContext = _context.NameAddress.Include(n => n.ProvinceCodeNavigation);
            return View(await clubsContext.ToListAsync());
        }

        // GET: MMNameAddress/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nameAddress = await _context.NameAddress
                .Include(n => n.ProvinceCodeNavigation)
                .FirstOrDefaultAsync(m => m.NameAddressId == id);
            if (nameAddress == null)
            {
                return NotFound();
            }

            return View(nameAddress);
        }

        // GET: MMNameAddress/Create
        public IActionResult Create()
        {
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode");
            return View();
        }

        // POST: MMNameAddress/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NameAddressId,FirstName,LastName,CompanyName,StreetAddress,City,PostalCode,ProvinceCode,Email,Phone")] MMNameAddressMetaData nameAddress)
        {
            try
            {

                    if (ModelState.IsValid)
                    {

                        NameAddress data = new NameAddress();
                        data.CompanyName = MMStringManipulation.MMCapitalize(nameAddress.CompanyName);
                        data.FirstName = MMStringManipulation.MMCapitalize(nameAddress.FirstName);
                        data.LastName = MMStringManipulation.MMCapitalize(nameAddress.LastName);
                        data.StreetAddress = MMStringManipulation.MMCapitalize(nameAddress.StreetAddress);
                        data.City = MMStringManipulation.MMCapitalize(nameAddress.City);
                        data.PostalCode = nameAddress.PostalCode;
                        data.ProvinceCode = nameAddress.ProvinceCode;
                        data.Email = nameAddress.Email;
                    string phoneFormat = "###-###-####";
                        data.Phone = Convert.ToInt64(nameAddress.Phone).ToString(phoneFormat);//nameAddress.Phone;
                    _context.NameAddress.Add(data);
                        //_context.Add(nameAddress);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }
                    ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", nameAddress.ProvinceCode);
                    TempData["SuccessMsg"] = "Member added successfully.";
                    return View(nameAddress);
                
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("Exception",ex.ToString());
              //  ViewData["ErrorMsg"] = ModelState.;
                return RedirectToAction("Index");
            }
        }

        // GET: MMNameAddress/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nameAddress = await _context.NameAddress.FindAsync(id);
            if (nameAddress == null)
            {
                return NotFound();
            }
            nameAddress.Phone = MMStringManipulation.MMExtraDigits(nameAddress.Phone);
            MMNameAddressMetaData obj = new MMNameAddressMetaData();
            obj.NameAddressId = nameAddress.NameAddressId;
            obj.FirstName = nameAddress.FirstName;
            obj.LastName = nameAddress.LastName;
            obj.Email = nameAddress.Email;
            obj.StreetAddress = nameAddress.StreetAddress;
            obj.City = nameAddress.City;
            obj.PostalCode = nameAddress.PostalCode;
            obj.ProvinceCode = nameAddress.ProvinceCode;
            obj.Phone = nameAddress.Phone;
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", nameAddress.ProvinceCode);
            return View(obj);
        }

        // POST: MMNameAddress/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NameAddressId,FirstName,LastName,CompanyName,StreetAddress,City,PostalCode,ProvinceCode,Email,Phone")] MMNameAddressMetaData nameAddress)
        {
            if (id != nameAddress.NameAddressId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    NameAddress data = new NameAddress();
                    data.NameAddressId = nameAddress.NameAddressId;
                    data.CompanyName = MMStringManipulation.MMCapitalize(nameAddress.CompanyName);
                    data.FirstName = MMStringManipulation.MMCapitalize(nameAddress.FirstName);
                    data.LastName = MMStringManipulation.MMCapitalize(nameAddress.LastName);
                    data.StreetAddress = MMStringManipulation.MMCapitalize(nameAddress.StreetAddress);
                    data.City = MMStringManipulation.MMCapitalize(nameAddress.City);
                    data.PostalCode = nameAddress.PostalCode;
                    data.ProvinceCode = nameAddress.ProvinceCode;
                    data.Email = nameAddress.Email;

                    string phoneFormat = "###-###-####";
                    data.Phone = Convert.ToInt64(nameAddress.Phone).ToString(phoneFormat);//nameAddress.Phone;
                    _context.Update(data);
                    await _context.SaveChangesAsync();
                    TempData["SuccessMsg"] = "Member updated successfully.";
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NameAddressExists(nameAddress.NameAddressId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProvinceCode"] = new SelectList(_context.Province, "ProvinceCode", "ProvinceCode", nameAddress.ProvinceCode);
            return View(nameAddress);
        }

        // GET: MMNameAddress/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nameAddress = await _context.NameAddress
                .Include(n => n.ProvinceCodeNavigation)
                .FirstOrDefaultAsync(m => m.NameAddressId == id);
            if (nameAddress == null)
            {
                return NotFound();
            }

            return View(nameAddress);
        }

        // POST: MMNameAddress/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var nameAddress = await _context.NameAddress.FindAsync(id);
                _context.NameAddress.Remove(nameAddress);
                await _context.SaveChangesAsync();
                TempData["SuccessMsg"] = "Member deleted successfully.";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                var exc = ex.ToString();
                if (ex.InnerException == null)
                {
                    TempData["DelExp"] = ex.Message;
                }
                else
                {
                    TempData["DelExp"] = ex.InnerException.ToString();
                }

                return RedirectToAction("Delete");
            }
        }
        #endregion
        #region Private Method
        private bool NameAddressExists(int id)
        {
            return _context.NameAddress.Any(e => e.NameAddressId == id);
        }
        #endregion
    }
}
