﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using A1Clubs.Models;
using Microsoft.AspNetCore.Http;

namespace A1Clubs.Controllers
{
    public class HomeController : Controller
    {
        #region Private Object
        private readonly ILogger<HomeController> _logger;
        #endregion
        #region Constructor

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        #endregion
        #region Action
        public IActionResult Index()
        {
            HttpContext.Session.SetString("Name","Mayank MakWana"); // initialized in controller
            TempData["Name"] = HttpContext.Session.GetString("Name");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}
